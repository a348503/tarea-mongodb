/*
Tarea bases de datos no relacionales usando mongodb

1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json
    EL resultado del comando fue:
    2023-10-12T16:40:46.169-0600	connected to: mongodb://localhost/
    2023-10-12T16:40:46.260-0600	800 document(s) imported successfully. 0 document(s) failed to import.

2) El conjunto de datos contiene 4 calificaciones de n estudiantes. 
Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: 
>use students; >db.grades.count() 
¿Cuántos registros arrojo el comando count?
    800

3) Encuentra todas las calificaciones del estudiante con el id numero 4.
    Secuencia usada: > db.grades.find({student_id: 4})

    Resultado:
        [
        {
            _id: ObjectId("50906d7fa3c412bb040eb587"),
            student_id: 4,
            type: 'exam',
            score: 87.89071881934647
        },
        {
            _id: ObjectId("50906d7fa3c412bb040eb588"),
            student_id: 4,
            type: 'quiz',
            score: 27.29006335059361
        },
        {
            _id: ObjectId("50906d7fa3c412bb040eb589"),
            student_id: 4,
            type: 'homework',
            score: 5.244452510818443
        },
        {
            _id: ObjectId("50906d7fa3c412bb040eb58a"),
            student_id: 4,
            type: 'homework',
            score: 28.656451042441
        }
        ]

4) ¿Cuántos registros hay de tipo exam?
    Secuencia utilizada: > db.grades.count({type: 'exam'})
    
    Resultado: 200

5) ¿Cuántos registros hay de tipo homework?
    Secuencia utilizada: > db.grades.count({type: 'homework'})
    
    Resultado: 400

6) ¿Cuántos registros hay de tipo quiz?
    Secuencia utilizada: > db.grades.count({type: 'quiz'})

    Resultado: 200

7) Elimina todas las calificaciones del estudiante con el id numero 3
    Secuencia utilizada: > db.grades.remove({student_id : 3})
    También pudo ser utilizada >b.grades.deleteMany({student_id : 3}) ya que remove está obsoleta.

    Resultado:
    { acknowledged: true, deletedCount: 4 }

8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
    Secuencia utilizada: db.grades.find({score : 75.29561445722392, type: 'homework'})

    Resultado:
    [
    {
        _id: ObjectId("50906d7fa3c412bb040eb59e"),
        student_id: 9,
        type: 'homework',
        score: 75.29561445722392
    }
    ]
    El estudiante que obtuvo esa calificación en una tarea fue el estudiante número 9.

9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
    Secuencia utilizada:  
    > db.grades.updateOne({_id:ObjectId("50906d7fa3c412bb040eb591")},{$set:{score:100}})

    Resultado: 
        {
        acknowledged: true,
        insertedId: null,
        matchedCount: 1,
        modifiedCount: 1,
        upsertedCount: 0
        }


10) A qué estudiante pertenece esta calificación.
    Secuencia utilizada: > db.grades.findOne({_id: ObjectId("50906d7fa3c412bb040eb591")})

    Resultado:
        {
        _id: ObjectId("50906d7fa3c412bb040eb591"),
        student_id: 6,
        type: 'homework',
        score: 100
        }

    Al estudiante número 6. 
















*/