# Tarea | Mongodb 101

Ésta tarea es una serie de pequeños ejercicios para adentrarnos en el uso de bases
de datos no relacionales. Consta de 10 ejercicios de los cuales los dos primeros son
más enfocados a aprender a conectarnos a la base de datos y el resto a operarciones crud
básicas.


## Autores

* **Gilberto Contreras Conn**

## Acknowledgments

* Profesor: I. S. Luis Antonio Rámirez Martínez
